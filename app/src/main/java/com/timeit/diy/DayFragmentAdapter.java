package com.timeit.diy;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class DayFragmentAdapter extends FragmentPagerAdapter {
  private Time time;
  private TaskDao dao;
  public DayFragmentAdapter (FragmentManager manager, Time time, TaskDao dao) {
    super(manager);
    this.time = time;
    this.dao = dao;
  }
  @Override
  public DayFragment getItem (int position) {
    Time newtime = new Time(time.getDay() + position, time.getMonth(), time.getYear());
    Task[] tasks = dao.getDay(newtime.getDay(), newtime.getMonth()+1, newtime.getYear());
    return DayFragment.newInstance(newtime, tasks);
  }
  @Override
  public int getCount () {
    return 20000;
  }
}
