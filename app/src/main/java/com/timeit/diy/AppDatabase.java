package com.timeit.diy;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.Executors;

@Database(entities = {Task.class,Settings.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
  public abstract TaskDao taskDao();
  public abstract SettingsDao settingsDao();
  private static AppDatabase INSTANCE;
  public static AppDatabase getDatabase (final Context context) {
    if (INSTANCE == null) {
      INSTANCE =
        Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "timeit")
        .allowMainThreadQueries()
          .addCallback(new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
              super.onCreate(db);
              Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                @Override
                public void run() {
                  AppDatabase.getDatabase(context).taskDao().insertAll(HOLIDAYS);
                }
              });
            }
          }).build();
    }
    return INSTANCE;
  }
  public static void destroyInstance () {
    INSTANCE = null;
  }

  private static Task[] HOLIDAYS = {
    new Task(new Time(1 , 1, 2018),new Time(1 , 1, 2018),"New Year's Day", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(16, 2, 2018),new Time(16, 2, 2018),"Chinese Lunar New Year's Day", "Special Non-working Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(25, 2, 2018),new Time(25, 2, 2018),"People Power Anniversary", "Observance", "", false, false, Task.HOLIDAY),
    new Task(new Time(20, 3, 2018),new Time(20, 3, 2018),"March equinox ", "Season", "", false, false, Task.HOLIDAY),
    new Task(new Time(29, 3, 2018),new Time(29, 3, 2018),"Maundy Thursday ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(30, 3, 2018),new Time(30, 3, 2018),"Good Friday ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(1 , 4, 2018),new Time(1 , 4, 2018),"Easter Sunday ", "Observance", "", false, false, Task.HOLIDAY),
    new Task(new Time(9 , 4, 2018),new Time(9 , 4, 2018),"The Day of Valor", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(13, 4, 2018),new Time(13, 4, 2018),"Lailatul Isra Wal Mi Raj", "Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(1 , 5, 2018),new Time(1 , 5, 2018),"Labor Day ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(12, 6, 2018),new Time(12, 6, 2018),"Independence Day", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(16, 6, 2018),new Time(16, 6, 2018),"Eidul-Fitar ", "Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(21, 6, 2018),new Time(21, 6, 2018),"June Solstice ", "Season", "", false, false, Task.HOLIDAY),
    new Task(new Time(21, 7, 2018),new Time(21, 7, 2018),"Ninoy Aquino Day", "Special Non-working Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(22, 8, 2018),new Time(22, 8, 2018),"Eid al-Adha ", "Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(23, 8, 2018),new Time(23, 8, 2018),"Eid al-Adha Day 2 ", "Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(27, 8, 2018),new Time(27, 8, 2018),"National Heroes Day holiday ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(12, 9, 2018),new Time(12, 9, 2018),"Amun Jadid", "Muslim, Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(23, 9, 2018),new Time(23, 9, 2018),"September equinox ", "Season", "", false, false, Task.HOLIDAY),
    new Task(new Time(1 , 10, 2018),new Time(1 , 10, 2018),"All Saints' Day ", "Special Non-working Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(2 , 10, 2018),new Time(2 , 10, 2018),"All Souls' Day", "Observance", "", false, false, Task.HOLIDAY),
    new Task(new Time(21, 10, 2018),new Time(21, 10, 2018),"Maulid un-Nabi", "Common Local holidays", "", false, false, Task.HOLIDAY),
    new Task(new Time(30, 10, 2018),new Time(30, 10, 2018),"Bonifacio Day ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(8 , 11, 2018),new Time(8 , 11, 2018),"Feast of the Immaculate Conception", "Observance", "", false, false, Task.HOLIDAY),
    new Task(new Time(21, 11, 2018),new Time(21, 11, 2018),"December Solstice ", "Season", "", false, false, Task.HOLIDAY),
    new Task(new Time(24, 11, 2018),new Time(24, 11, 2018),"Christmas Eve ", "Observance", "", false, false, Task.HOLIDAY),
    new Task(new Time(25, 11, 2018),new Time(25, 11, 2018),"Christmas Day ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(30, 11, 2018),new Time(30, 11, 2018),"Rizal Day ", "Regular Holiday", "", false, false, Task.HOLIDAY),
    new Task(new Time(31, 11, 2018),new Time(31, 11, 2018),"New Year's Eve", "Special Non-working Holiday", "", false, false, Task.HOLIDAY)
  };
}
