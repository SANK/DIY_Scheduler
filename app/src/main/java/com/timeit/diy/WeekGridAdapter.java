package com.timeit.diy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;
import static java.lang.String.valueOf;

public class WeekGridAdapter extends BaseAdapter {
  private Task[] tasks;
  private Context context;
  private Time time;
  public WeekGridAdapter (Context context, Time time, Task[] tasks) {
    this.context = context;
    this.tasks = tasks;
    this.time = time;
  }

  @Override
  public int getCount () { return 8*24; } // 7 days 24 hours and one time column
  @Override
  public long getItemId (int position) { return position; }
  @Override
  public Object getItem (int position) { return null; }
  @Override
  public View getView (int position, View view, ViewGroup group) {
    if (null == view) {
      LayoutInflater layout = LayoutInflater.from(context);
      view = layout.inflate(R.layout.adapter_week, null);
    }

    TextView text = view.findViewById(R.id.hour);
    if (position % 8 == 0) {
      text.setText(((position / 8) % 12) + ":00");
    } else {
      view.setBackgroundColor(context.getResources().getColor(android.R.color.background_light));
      final int n = position; // convenience
      final int k = (n + (8 - (n % 8))) / 8; // get the index starting from 1
      final int hour = k - 1; // convert to hour starting from midnight
      final int index = n - k; // get the day cell index
      final int weekday = index % 7; // get the week day from cell position
      text.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Log.i("WeekGridAdapter", "switching");
          Intent intent = new Intent();
          intent.putExtra("kind", 2); // SWITCH
          intent.putExtra("activity", 1); // DAY
          intent.putExtra("time", Time.newWeek(weekday, time.getWeek(), time.getYear()));
          ((ViewsActivity)context).setResult(RESULT_OK, intent);
        }
      });
      Log.i("WeekGridAdapter", "tasks: " + tasks.length);
      String[] day = {"S","M", "T", "W", "T", "F", "S"};
      text.setText(day[weekday]);
      for (Task elem : tasks) {
        int week = elem.getFrom().getWeekDay();
        int from = elem.getFrom().getHour();
        int to   = elem.getTo().getHour();
        if (week == weekday && ((from <= hour && to >= hour) || elem.getKind() == Task.HOLIDAY)) {
          switch (elem.getKind()) {
            case Task.HOLIDAY: view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_green_light)); break;
            case Task.LESSON: view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_blue_light)); break;
            case Task.LESSON+1: view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_orange_light)); break;
            case Task.LESSON+2: view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_purple)); break;
            case Task.LESSON+3: view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_red_light)); break;
            default:view.setBackgroundColor(view.getResources().getColor(android.R.color.holo_red_light));
          }
        }
      }
    }
    return view;
  }
}
