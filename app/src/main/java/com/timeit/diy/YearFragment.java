package com.timeit.diy;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

public class YearFragment extends Fragment {
  public Context context;
  public static final YearFragment newInstance (Time time, Task[] tasks) {
    YearFragment fragment = new YearFragment();
    Bundle arguments = new Bundle();
    arguments.putParcelable("time", time);
    arguments.putParcelableArray("tasks", tasks);
    fragment.setArguments(arguments);
    return fragment;
  }
  @Override
  public void onAttach (Context context) {
    super.onAttach(context);
    this.context = context;
  }
  public View onCreateView (LayoutInflater inflater, ViewGroup group, Bundle instance) {
    Time time = getArguments().getParcelable("time"); // Just crash if null
    Task[] tasks = (Task[]) getArguments().getParcelableArray("tasks");

    View view = inflater.inflate(R.layout.fragment_year, group, false);

    TextView text = view.findViewById(R.id.year);
    GridView grid = view.findViewById(R.id.grid);

    text.setText("Year " + time.getYear());
    grid.setAdapter(new YearMonthGridAdapter(context, time, tasks));
    return view;
  }
}

