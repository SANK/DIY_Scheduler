package com.timeit.diy;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;
import static com.timeit.diy.R.color.colorPrimary;

public class MonthGridAdapter extends BaseAdapter {
  private Time time;
  private Task[] tasks;
  private Context context;
  public MonthGridAdapter (Context context, Time time, Task[] tasks) {
    this.time = time;
    this.tasks = tasks;
    this.context = context;
  }
  @Override
  public int getCount () { return this.time.getLength() + 3 + time.getWeekDay(); }
  @Override
  public long getItemId (int position) { return position; }
  @Override
  public Object getItem (int position) { return null; }
  @Override
  public View getView (final int position, View view, ViewGroup group) {
    if (null == view) {
      LayoutInflater layout = LayoutInflater.from(context);
      view = layout.inflate(R.layout.adapter_month, null);
    }

    TextView text = view.findViewById(R.id.day);
    TextView dot = view.findViewById(R.id.dot);
    if (time.getWeekDay() + 2 < position) {
      view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Intent intent = new Intent();
          intent.putExtra("time", new Time(position-(2+time.getWeekDay()), time.getMonth(), time.getYear()));
          intent.putExtra("kind", 2); // SWITCH
          intent.putExtra("activity", DAY);
          ((ViewsActivity)context).setResult(RESULT_OK, intent);
          ((ViewsActivity)context).finish();
        }
      });
      text.setText(String.valueOf(position - (2+time.getWeekDay())));
      for (Task elem : tasks) {
        if (elem.getFrom().getDay() == (position - (2+time.getWeekDay()))) {
          switch (elem.getKind()) {
            case Task.LESSON: dot.setBackgroundColor(view.getResources().getColor(android.R.color.holo_blue_bright)); break;
            case Task.LESSON+1: dot.setBackgroundColor(view.getResources().getColor(android.R.color.holo_blue_bright)); break;
            case Task.LESSON+2: dot.setBackgroundColor(view.getResources().getColor(android.R.color.holo_purple));
            case Task.LESSON+3: dot.setBackgroundColor(view.getResources().getColor(android.R.color.holo_green_light)); break;
            default: dot.setBackgroundColor(view.getResources().getColor(android.R.color.holo_green_dark)); break;
          }
        }
      }
    } else {
      text.setText("");
    }
    return view;
  }
    public final int
    NONE = 0,
    DAY = 1,
    WEEK = 2,
    MONTH = 3,
    YEAR = 4,
    HOLIDAY = 5,
    TASK = 6,
    SETTINGS = 7,
    ABOUT = 8,
    LESSON = 9;

}
