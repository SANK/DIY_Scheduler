package com.timeit.diy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;
import static com.timeit.diy.R.color.colorPrimary;

public class YearMonthGridAdapter extends BaseAdapter {
  private Time time;
  private Task[] tasks;
  private Context context;
  public YearMonthGridAdapter(Context context, Time time, Task[] tasks) {
    this.time = time;
    this.tasks = tasks;
    this.context = context;
  }
  @Override
  public int getCount () { return this.time.getLength() + 3 + time.getWeekDay(); }
  @Override
  public long getItemId (int position) { return position; }
  @Override
  public Object getItem (int position) { return null; }
  @Override
  public View getView (final int position, View view, ViewGroup group) {
    if (null == view) {
      LayoutInflater layout = LayoutInflater.from(context);
      view = layout.inflate(R.layout.adapter_year, null);
    }

    GridView grid = view.findViewById(R.id.grid);
    grid.setAdapter(new YearGridAdapter(context, time, tasks));
    grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView adapt, View view, int k, long l) {
        Intent intent = new Intent();
        intent.putExtra("time", new Time(0, position, time.getYear()));
        intent.putExtra("kind", 2); // SWITCH
        intent.putExtra("activity", 3); // MONTH
        ((ViewsActivity)context).setResult(RESULT_OK, intent);
      }
    });
    return view;
  }
}
