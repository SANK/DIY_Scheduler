package com.timeit.diy;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
  private boolean exit = false;
  private static int SPLASH_TIME_OUT = 2000;
  private Settings settings; // set this
  private Task[] tasks = {};
  private AppDatabase database;
  private Time time;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    // Retrive database
    this.database = AppDatabase.getDatabase(this);
    this.settings = AppDatabase.getDatabase(this).settingsDao().getSettings();

    // Get the current time and offset by a year to allow for scrollback
    time = new Time(Calendar.getInstance());

    RelativeLayout rel = findViewById(R.id.layout);
    rel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        act(MONTH);
      }
    });

    new Handler().postDelayed(new Runnable(){
      @Override
      public void run(){
        act(last);
      }
    },SPLASH_TIME_OUT);
  }

  // Message tags for matching against on user input and process result
  public final int
    NONE = 0,
    DAY = 1,
    WEEK = 2,
    MONTH = 3,
    YEAR = 4,
    HOLIDAY = 10,
    TASK = 6,
    SETTINGS = 7,
    ABOUT = 8,
    LESSON = 9;

  private int last = MONTH;
  // Act on messages passed through from both activities and user
  private void act (int id) {
    if (id >= DAY && id <= TASK) {
      // If a view defined by a viewpager is selected we start the general view activity
      this.last = id;
      Intent intent = new Intent(this, ViewsActivity.class);
      intent.putExtra("fragment", id);
      intent.putExtra("time", this.time);
      Log.i("Main", "starting fragment " + id);
      startActivityForResult(intent, id);
      overridePendingTransition(R.transition.fade_out, R.transition.fade_in);
    } else {
      // Otherwise we check which activity we should start and run it
      Intent intent = null;
      switch (id) {
        case HOLIDAY: intent = new Intent(this, HolidayActivity.class); break;
        case SETTINGS: intent = new Intent(this, SettingsActivity.class);
          intent.putExtra("settings", settings); break;
        case LESSON: intent = new Intent(this, LessonActivity.class); break;
        case ABOUT: intent = new Intent(this, AboutActivity.class); break;
        default: // do nothing as NONE/RETURN
      }
      intent.putExtra("time", this.time);
      startActivityForResult(intent, id);
    }
  }

  public final int RETURN = 0, NEWTASK = 1, SWITCH = 2, EXIT = 3;

  @Override
  public void onActivityResult(int request, int result, Intent data) {
    // Once a process returns we retrieve the `kind` which tells us what the message contains
    super.onActivityResult(request, result, data);
    if (request >= DAY && request <= TASK && RESULT_OK == result) {
      Bundle bundle = data.getExtras();
      switch (bundle.getInt("kind")) {
        case LESSON:
          this.time = bundle.getParcelable("time");
          this.time = new Time(this.time.getDay(), this.time.getMonth(), this.time.getYear());
          act(LESSON);
        case SWITCH:
          this.time = bundle.getParcelable("time");
          this.time = new Time(this.time.getDay(), this.time.getMonth(), this.time.getYear());
          act(bundle.getInt("activity"));
        default: // impossible unless NONE/RETURN which we only recover the time
          this.time = bundle.getParcelable("time");
          this.time = new Time(this.time.getDay(), this.time.getMonth(), this.time.getYear());
      }
    } else {
      switch (request) {
        case SETTINGS:
          if (result == RESULT_OK) {
            this.settings = data.getExtras().getParcelable("settings");
            AppDatabase.getDatabase(this).settingsDao().insert(this.settings);
            if (settings.getTheme() == 1) setTheme(android.R.style.Theme_Holo);
            else setTheme(android.R.style.Theme_Holo_Light);
            this.recreate();
          }
          break;
        case LESSON:
          if (result == RESULT_OK) {
            database.taskDao().insertAll((Task) data.getExtras().getParcelable("task"));
          }
          act(last);
        case ABOUT: break; // ignore as no result will ever be returned
        default: // impossible
      }
    }
  }

  @Override
  public void onBackPressed () {
    if (exit) {
      super.onBackPressed();
    } else {
      Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
      this.exit = true;
    }
  }
}
