package com.timeit.diy;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LessonActivity extends AppCompatActivity {
  private EditText lesson, teacher, location, to_date, from_date;
  private TextView savebutton, blue, orange, purple, red;
  public static int color = 3;
  protected void onCreate (Bundle instance) {
    super.onCreate(instance);
    setContentView(R.layout.activity_lesson);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    this.lesson = findViewById(R.id.lesson);
    this.teacher = findViewById(R.id.teacher);
    this.location = findViewById(R.id.location);
    this.from_date = findViewById(R.id.from_date);
    this.to_date = findViewById(R.id.to_date);
    this.savebutton = findViewById(R.id.save);
    this.blue = findViewById(R.id.blue);
    this.red = findViewById(R.id.red);
    this.purple = findViewById(R.id.purple);
    this.orange = findViewById(R.id.orange);
    savebutton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        save();
      }
    });
    blue.setOnClickListener(makeListener(3));
    orange.setOnClickListener(makeListener(4));
    purple.setOnClickListener(makeListener(5));
    red.setOnClickListener(makeListener(6));
  }

  private View.OnClickListener makeListener (final int i) {
    return new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        LessonActivity.color = i;
      }
    };
  }
  @Override
  public void onBackPressed () {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("New Lesson")
      .setMessage("Are you sure you want to discard this lesson?")
      .setPositiveButton("Keep", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {}
      })
      .setNegativeButton("Discard", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          setResult(RESULT_CANCELED);
          finish();
        }
      }).show();
  }
  private void save() {
    SimpleDateFormat format = new SimpleDateFormat("hh:mm dd/MM/yyyy");
    Calendar calendar = Calendar.getInstance();
    Task task = new Task();
    Date date = null;
    boolean failed = false;
    Time from = null, to = null;

    try {
      date = format.parse(from_date.getText().toString());
    } catch (ParseException e) {
      Toast.makeText(this, "Please correct the from date format", Toast.LENGTH_SHORT).show();
      failed = true;
    }

    if (!failed) {
      calendar.setTime(date);
      from = new Time(calendar);
    }

    try {
      date = format.parse(to_date.getText().toString());
    } catch (ParseException e) {
      Toast.makeText(this, "Please correct the to date format", Toast.LENGTH_SHORT).show();
      failed = true;
    }

    if (!failed) {
      calendar.setTime(date);
      to = new Time(calendar);
    }

    if (!failed) {
      task.setFrom(new Time (from.getDay(), from.getMonth(), from.getYear()));
      task.setTo(new Time(to.getDay(), to.getMonth(), to.getYear()));
      task.setName(lesson.getText().toString());
      task.setDescription(teacher.getText().toString());
      task.setLocation(location.getText().toString());
      task.setCompleted(false);
      task.setKind(this.color);

      Intent intent = new Intent();
      intent.putExtra("task", task);
      setResult(RESULT_OK, intent);
      finish();
    }
  }
}
