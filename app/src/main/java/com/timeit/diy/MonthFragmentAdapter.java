package com.timeit.diy;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MonthFragmentAdapter extends FragmentPagerAdapter {
  private Time time;
  private TaskDao dao;
  MonthFragmentAdapter (FragmentManager manager, Time time, TaskDao dao) {
    super(manager);
    this.time = time;
    this.dao = dao;
  }
  @Override
  public MonthFragment getItem (int position) {
    Time newtime = new Time(time.getDay(), time.getMonth() + position, time.getYear());
    Task[] tasks = dao.getMonth(newtime.getMonth()+1, newtime.getYear());
    return MonthFragment.newInstance(newtime, tasks);
  }
  @Override
  public int getCount () {
    return 20000; // A user going past this would be absurd. Amusing but absurd.
  }
}