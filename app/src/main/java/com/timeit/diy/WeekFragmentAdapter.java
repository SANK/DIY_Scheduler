package com.timeit.diy;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class WeekFragmentAdapter extends FragmentPagerAdapter {
  private Time time;
  private Task[] tasks;
  private TaskDao dao;
  public WeekFragmentAdapter(FragmentManager manager, Time time, TaskDao dao) {
    super(manager);
    this.time = time;
    this.dao = dao;
  }
  @Override
  public WeekFragment getItem (int position) {
    Time newtime = Time.newWeek(time.getDay(), time.getWeek() + position, time.getYear());
    Task[] tasks = dao.getWeek(newtime.getWeek()+1, newtime.getYear());
    Log.i("WeekAdapter", "item created");
    return WeekFragment.newInstance(newtime, tasks);
  }
  @Override
  public int getCount () {
    return 20000;
  }
}
