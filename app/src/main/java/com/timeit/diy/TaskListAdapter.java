package com.timeit.diy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static java.lang.String.format;

public class TaskListAdapter extends BaseAdapter {
  private Task[] tasks;
  private Context context;
  public TaskListAdapter (Context context, Task[] tasks) {
    this.context = context;
    this.tasks = tasks;
  }
  @Override
  public int getCount () { return tasks.length; }
  @Override
  public long getItemId (int position) { return position; }
  @Override
  public Object getItem (int position) { return tasks[position]; }
  @Override
  public View getView (int position, View view, ViewGroup group) {
    if (null == view) {
      LayoutInflater layout = LayoutInflater.from(context);
      view = layout.inflate(R.layout.adapter_tasks, null);
    }

    TextView name = view.findViewById(R.id.name);
    TextView description = view.findViewById(R.id.desc);
    TextView due = view.findViewById(R.id.due);
    TextView from = view.findViewById(R.id.from);
    Time duetime = tasks[position].getTo();
    Time fromtime = tasks[position].getFrom();

    name.setText(tasks[position].getName());
    description.setText(tasks[position].getDescription());
    from.setText(format("from: %d/%d/%d at %d:%d", fromtime.getDay(), fromtime.getMonth(), fromtime.getYear(), fromtime.getHour(), fromtime.getMinute()));
    due.setText(format("to: %d/%d/%d at %d:%d", duetime.getDay(), duetime.getMonth(), duetime.getYear(), duetime.getHour(), duetime.getMinute()));
    return view;
  }
}
