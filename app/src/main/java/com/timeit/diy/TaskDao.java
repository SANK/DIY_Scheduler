package com.timeit.diy;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface TaskDao {
  @Query("SELECT * FROM tasks")
  Task[] getAll();
  @Query("SELECT * FROM tasks WHERE name LIKE :name LIMIT 1")
  Task findByName(String name);
  @Query("SELECT * FROM tasks WHERE from_year IS :year")
  Task[] getYear(int year);
  @Query("SELECT * FROM tasks WHERE kind IS 2")
  Task[] getHolidays();
  @Query("SELECT * FROM tasks WHERE from_month IS :month AND (from_year IS :year OR kind IS 2)")
  Task[] getMonth(int month, int year);
  @Query("SELECT * FROM tasks WHERE from_week IS :week AND (from_year IS :year OR kind IS 2)")
  Task[] getWeek(int week, int year);
  @Query("SELECT * FROM tasks WHERE from_month IS :month AND (from_year IS :year OR kind IS 2) AND from_day IS :day")
  Task[] getDay(int day, int month, int year);
  @Query("SELECT COUNT(*) FROM tasks")
  int getSize();
  @Insert(onConflict = REPLACE)
  void insertAll(Task... tasks);
  @Delete
  void delete(Task task);
}
