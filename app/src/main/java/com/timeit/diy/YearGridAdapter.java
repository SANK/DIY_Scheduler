package com.timeit.diy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;
import static com.timeit.diy.R.color.colorPrimary;

public class YearGridAdapter extends BaseAdapter {
  private Time time;
  private Task[] tasks;
  private Context context;
  public YearGridAdapter(Context context, Time time, Task[] tasks) {
    this.time = time;
    this.tasks = tasks;
    this.context = context;
  }
  @Override
  public int getCount () { return this.time.getLength() + 3 + time.getWeekDay(); }
  @Override
  public long getItemId (int position) { return position; }
  @Override
  public Object getItem (int position) { return null; }
  @Override
  public View getView (final int position, View view, ViewGroup group) {
    if (null == view) {
      LayoutInflater layout = LayoutInflater.from(context);
      view = layout.inflate(R.layout.adapter_year_grid, null);
    }

    TextView text = view.findViewById(R.id.day);
    if (time.getWeekDay() + 2 < position) {
      text.setText(String.valueOf(position - (2+time.getWeekDay())));
    } else {
      text.setText("");
    }
    return view;
  }
}
