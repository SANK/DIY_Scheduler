package com.timeit.diy;

import android.arch.persistence.room.*;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "settings")
public class Settings implements Parcelable {
  @PrimaryKey
  private final int key = 0;
  @ColumnInfo(name = "theme")
  private int theme;
  @ColumnInfo(name ="view")
  private int default_view;
  @ColumnInfo(name = "notify")
  private boolean notifications;
  @ColumnInfo(name = "days")
  private int enabled_days;

  Settings (int theme, int default_view, boolean notifications, int enabled_days) {
    this.theme = theme;
    this.default_view = default_view;
    this.notifications = notifications;
    this.enabled_days = enabled_days;
  }

  public int getKey () { return key; }
  public int getTheme             () { return this.theme; }
  public int getDefaultView       () { return this.default_view; }
  public boolean getNotifications () { return this.notifications; }
  public int getEnabledDays       () { return this.enabled_days; }
  public int getDefault_view      () { return this.default_view; }
  public int getEnabled_days      () { return this.enabled_days; }

  public void setKey (int key) {}
  public void setTheme (int theme) { this.theme = theme; }
  public void setDefaultView (int default_view) { this.default_view = default_view; }
  public void setNotifications (boolean notifications) { this.notifications = notifications; }
  public void setEnabledDays (int enabled) { this.enabled_days = enabled_days; }
  public void setDefault_view (int view) { this.default_view = view; }
  public void setEnabled_days (int view) { this.enabled_days = view; }

  @Ignore
  @Override
  public void writeToParcel (Parcel out, int flags) {
    out.writeInt (this.theme);
    out.writeInt (this.default_view);
    out.writeInt (this.notifications ? 1 : 0);
    out.writeInt (this.enabled_days);
  }

  @Ignore
  private Settings (Parcel in) {
    this.theme         = in.readInt();
    this.default_view  = in.readInt();
    this.notifications = in.readInt() == 1;
    this.enabled_days  = in.readInt();
  }

  @Ignore
  public static final Creator<Settings> CREATOR =
    new Creator<Settings>() {
    public Settings createFromParcel (Parcel in) {
      return new Settings(in);
    }
    public Settings[] newArray (int size) {
      return new Settings[size];
    }
  };

  @Ignore
  @Override
  public int describeContents () {
    return 0;
  }
}
