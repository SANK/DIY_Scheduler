package com.timeit.diy;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface SettingsDao {
  @Query("SELECT * FROM settings")
  Settings getSettings();
  @Insert(onConflict = REPLACE)
  void insert(Settings settings);
  @Delete
  void delete(Settings settings);
}
