package com.timeit.diy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class ViewsActivity extends AppCompatActivity {
  private AppDatabase database;
  private Time time;
  private int selectedfrag;
  @Override
  protected void onCreate (Bundle instance) {
    super.onCreate(instance);
    setContentView(R.layout.activity_views);

    Intent intent = getIntent();
    this.time = (Time) intent.getExtras().getParcelable("time");
    Time pastyear = new Time(time.getDay(), time.getMonth(), time.getYear() - 1);
    this.selectedfrag = intent.getExtras().getInt("fragment");

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    // Retrive database
    this.database = AppDatabase.getDatabase(this);

    // Setup viewpager based on the selected fragment
    ViewPager pager = findViewById(R.id.viewpager);
    switch (selectedfrag) {
      case DAY:
        pager.setAdapter(new DayFragmentAdapter(getSupportFragmentManager(), pastyear, database.taskDao()));
        pager.setCurrentItem(365);
        break;
      case WEEK:
        pager.setAdapter(new WeekFragmentAdapter(getSupportFragmentManager(), pastyear, database.taskDao()));
        pager.setCurrentItem(52);
        break;
      case MONTH:
        pager.setAdapter(new MonthFragmentAdapter(getSupportFragmentManager(), pastyear, database.taskDao()));
        pager.setCurrentItem(12);
        break;
      case YEAR:
        pager.setAdapter(new YearFragmentAdapter(getSupportFragmentManager(), time, database.taskDao()));
        pager.setCurrentItem(1);
        break;
      case HOLIDAY:
        //pager.setAdapter(new HolidayFragmentAdapter(getSupportFragmentManager(), time, database.taskDao()));
        break;
      case TASK:
        //pager.setAdapter(new TaskFragmentAdapter(getSupportFragmentManager(), time, database.taskDao()));
        break;
      default: // impossible
    }

    DrawerLayout drawer = findViewById(R.id.drawer);
    NavigationView nav  = findViewById(R.id.navigation);

    nav.setNavigationItemSelectedListener(
      new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
          // Bind an action to each option in the drawer
          act(selection(item));
          return true;
        }
      });
  }

  public final int RETURN = 0, NEWTASK = 1, SWITCH = 2;
  public final int
    NONE = 0,
    DAY = 1,
    WEEK = 2,
    MONTH = 3,
    YEAR = 4,
    HOLIDAY = 10,
    TASK = 6,
    SETTINGS = 7,
    ABOUT = 8,
    LESSON = 9;

  private void act (int id) {
    Intent intent = new Intent();
    intent.putExtra("kind", SWITCH);
    intent.putExtra("time", this.time);
    intent.putExtra("activity", id);
    setResult(RESULT_OK, intent);
    overridePendingTransition(R.transition.fade_out, R.transition.fade_in);
    finish();
  }

  private int selection (MenuItem item) {
    switch (item.getItemId()) {
      // Translate all nav actions to message tags
      case R.id.day: return DAY;
      case R.id.week: return WEEK;
      case R.id.month: return MONTH;
      case R.id.year: return YEAR;
      case R.id.holiday: return HOLIDAY;
      //case R.id.task: return TASK;
      case R.id.lesson: return LESSON;
      case R.id.settings: return SETTINGS;
      case R.id.about: return ABOUT;
      default: return NONE;
    }
  }

  @Override
  public void onBackPressed () {
    Intent intent = new Intent();
    intent.putExtra("kind", NONE);
    intent.putExtra("time", time);
    setResult(RESULT_OK, intent);
    overridePendingTransition(R.transition.fade_out, R.transition.fade_in);
    finish();
  }
}
