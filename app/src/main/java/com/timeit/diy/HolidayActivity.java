package com.timeit.diy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

public class HolidayActivity extends AppCompatActivity {
  private Settings settings;
  protected void onCreate (Bundle instance) {
    super.onCreate(instance);
    setContentView(R.layout.activity_holiday);
    ListView list = findViewById(R.id.listview);
    TextView text = findViewById(R.id.text);
    text.setText("Holidays");
    TaskDao dao = AppDatabase.getDatabase(this).taskDao();
    list.setAdapter(new TaskListAdapter(this, dao.getHolidays()));
  }
  @Override
  public void onBackPressed() {
    setResult(RESULT_CANCELED);
    finish();
  }
}
