package com.timeit.diy;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "tasks")
public class Task implements Parcelable {
  public static final int TASK = 0, HOLIDAY = 2, LESSON = 3; // Lesson color n+3
  @PrimaryKey(autoGenerate = true)
  private int tid;
  @Embedded(prefix = "from_")
  private Time from;
  @Embedded(prefix = "to_")
  private Time to;
  @ColumnInfo(name = "name")
  private String name;
  @ColumnInfo(name = "description")
  private String description;
  @ColumnInfo(name = "location")
  private String location;
  @ColumnInfo(name = "completed")
  private boolean completed;
  @ColumnInfo(name = "repeating")
  private boolean repeating;
  @ColumnInfo(name = "kind")
  private int kind;

  public Task () {}
  public Task (Time from, Time to, String name, String desc, String loc, boolean com, boolean rep, int kind) {
    this.from = from;
    this.to = to;
    this.name = name;
    this.description = desc;
    this.location = loc;
    this.completed = com;
    this.repeating = rep;
    this.kind = kind;
  }

  public int getTid () { return this.tid; }
  public Time getFrom () { return this.from; }
  public Time getTo () { return this.to; }
  public String getName        () { return this.name; }
  public String getDescription () { return this.description; }
  public String getLocation    () { return this.location; }
  public boolean getCompleted  () { return this.completed; }
  public boolean getRepeating  () { return this.repeating; }
  public int getKind           () { return this.kind; }

  @Ignore
  public void set(Time from, Time to, String name,
    String description, String location, boolean completed,
    boolean repeating, int kind) {
    this.from        = from;
    this.to          = to;
    this.name        = name;
    this.description = description;
    this.location    = location;
    this.completed   = completed;
    this.repeating   = repeating;
    this.kind        = kind;
  }
  public void setTid (int tid) { this.tid = tid; }
  public void setFrom (Time from) { this.from = from; }
  public void setTo (Time to) { this.to = to; }
  public void setName (String name) { this.name = name; }
  public void setDescription (String description) { this.description = description; }
  public void setLocation (String location) { this.location = location; }
  public void setCompleted (boolean completed) { this.completed = completed; }
  public void setRepeating (boolean repeating) { this.repeating = repeating; }
  public void setKind (int kind) { this.kind = kind; }

  @Ignore
  @Override
  public void writeToParcel (Parcel out, int flags) {
    out.writeParcelable (this.from, flags);
    out.writeParcelable (this.to, flags);
    out.writeString     (this.name);
    out.writeString     (this.description);
    out.writeString     (this.location);
    out.writeInt        (this.completed ? 1 : 0);
    out.writeInt        (this.repeating ? 1 : 0);
    out.writeInt        (this.kind);
  }

  @Ignore
  private Task (Parcel in) {
    this.from        = (Time) in.readParcelable(Time.class.getClassLoader());
    this.to          = (Time) in.readParcelable(Time.class.getClassLoader());
    this.name        = in.readString();
    this.description = in.readString();
    this.location    = in.readString();
    this.completed   = in.readInt() == 1;
    this.repeating   = in.readInt() == 1;
    this.kind        = in.readInt();
  }

  @Ignore
  public static final Creator<Task> CREATOR =
    new Creator<Task>() {
    public Task createFromParcel (Parcel in) {
      return new Task(in);
    }
    public Task[] newArray (int size) {
      return new Task[size];
    }
  };

  @Ignore
  @Override
  public int describeContents () {
    return 0;
  }
}
