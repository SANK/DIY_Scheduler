package com.timeit.diy;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class Time implements Parcelable {
  private static final int[] ends = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  private int minute;
  private int hour;
  private int day;
  private int week;
  private int month;
  private int year;
  private int week_day;
  public Time (int day, int month, int year) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(year, month, day, 0, 0, 0);
    fromCalendar(calendar);
  }

  public static Time newWeek (int day, int week, int year) {
    Calendar calendar = Calendar.getInstance();
    int yearday = (7 * week) - day;
    return new Time(yearday, 0, year);
  }

  public Time (Calendar calendar) {
    fromCalendar(calendar);
  }

  public int getMinute  () { return this.minute; }
  public int getHour    () { return this.hour; }
  public int getDay     () { return this.day; }
  public int getWeek    () { return this.week; }
  public int getMonth   () { return this.month; }
  public int getYear    () { return this.year; }
  public int getWeekDay () { return this.week_day; }
  public int getWeek_day () { return this.week_day; }
  public int getLength  () { return this.ends[month]; }

  public void setMinute (int minute) { this.minute = minute; }
  public void setHour (int hour) { this.hour = hour; }
  public void setWeek (int week) { this.week = week; }
  public void setWeek_day (int week) { this.week_day = week; }

  private void fromCalendar (Calendar calendar) {
    this.day      = calendar.get(Calendar.DAY_OF_MONTH);
    this.week     = calendar.get(Calendar.WEEK_OF_YEAR);
    this.month    = calendar.get(Calendar.MONTH);
    this.year     = calendar.get(Calendar.YEAR);
    this.week_day = calendar.get(Calendar.DAY_OF_WEEK);
  }

  @Override
  public void writeToParcel (Parcel out, int flags) {
    out.writeInt(this.minute);
    out.writeInt(this.hour);
    out.writeInt(this.day);
    out.writeInt(this.week);
    out.writeInt(this.month);
    out.writeInt(this.year);
    out.writeInt(this.week_day);
  }

  public Time (Parcel in) {
    this.minute   = in.readInt();
    this.hour     = in.readInt();
    this.day      = in.readInt();
    this.week     = in.readInt();
    this.month    = in.readInt();
    this.year     = in.readInt();
    this.week_day = in.readInt();
  }

  public static final Creator<Time> CREATOR =
    new Creator<Time>() {
    public Time createFromParcel (Parcel in) {
      return new Time(in);
    }
    public Time[] newArray (int size) {
      return new Time[size];
    }
  };


  @Override
  public int describeContents () {
    return 0;
  }
}
