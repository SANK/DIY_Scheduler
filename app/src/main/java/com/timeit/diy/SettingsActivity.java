package com.timeit.diy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.ToggleButton;

public class SettingsActivity extends AppCompatActivity {
    CheckBox notify ;
    CheckBox mon ;
    CheckBox tue ;
    CheckBox wed ;
    CheckBox thu ;
    CheckBox fri ;
    CheckBox sat ;
    CheckBox sun ;
    CheckBox dark ;
    private Settings settings;
  protected void onCreate (Bundle instance) {
    super.onCreate(instance);
    setContentView(R.layout.activity_settings);
    Intent intent = getIntent();
    this.settings = intent.getExtras().getParcelable("settings");
    if (settings == null) {
      settings = new Settings(0,3, false, 0);
    }
    notify = findViewById(R.id.notify);
    mon = findViewById(R.id.mon);
    tue = findViewById(R.id.tue);
    wed = findViewById(R.id.wed);
    thu = findViewById(R.id.thu);
    fri = findViewById(R.id.fri);
    sat = findViewById(R.id.sat);
    sun = findViewById(R.id.sun);
    dark = findViewById(R.id.theme);
    if (settings.getNotifications()) notify.toggle();
    if (settings.getTheme() == 1) notify.toggle();
    int days = settings.getEnabled_days();
    if ((days & 1) > 0) mon.toggle();
    if ((days & 2) > 0) tue.toggle();
    if ((days & 4) > 0) wed.toggle();
    if ((days & 8) > 0) thu.toggle();
    if ((days & 16) > 0) fri.toggle();
    if ((days & 32) > 0) sat.toggle();
    if ((days & 64) > 0) sun.toggle();
  }
  @Override
  public void onBackPressed () {
    int days = 0;
    days |= mon.isChecked()?1:0;
    days |= tue.isChecked()?2:0;
    days |= wed.isChecked()?4:0;
    days |= thu.isChecked()?8:0;
    days |= fri.isChecked()?16:0;
    days |= sat.isChecked()?32:0;
    days |= sun.isChecked()?64:0;
    settings.setEnabled_days(days);
    settings.setNotifications(notify.isChecked());
    settings.setTheme(dark.isChecked()?1:0);
    settings.setDefaultView(3);
    save();
  }
  private void save() {
    Intent intent = new Intent();
    intent.putExtra("settings", settings);
    setResult(RESULT_OK, intent);
    finish();
  }
}
