package com.timeit.diy;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class YearFragmentAdapter extends FragmentPagerAdapter {
  private Time time;
  private Task[] tasks;
  private TaskDao dao;
  public YearFragmentAdapter(FragmentManager manager, Time time, TaskDao dao) {
    super(manager);
    this.time = time;
    this.dao = dao;
  }
  @Override
  public YearFragment getItem (int position) {
    Time newtime = new Time(0, 0, time.getYear() + position);
    Task[] tasks = dao.getYear(newtime.getYear());
    Log.i("YearAdapter", "item created");
    return YearFragment.newInstance(newtime, tasks);
  }
  @Override
  public int getCount () {
    return 20000;
  }
}