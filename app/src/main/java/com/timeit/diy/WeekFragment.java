package com.timeit.diy;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import static java.lang.String.valueOf;

public class WeekFragment extends Fragment {
  public Context context;
  public static final WeekFragment newInstance (Time time, Task[] tasks) {
    WeekFragment fragment = new WeekFragment();
    Bundle arguments = new Bundle();
    arguments.putParcelable("time", time);
    arguments.putParcelableArray("tasks", tasks);
    fragment.setArguments(arguments);
    return fragment;
  }
  @Override
  public void onAttach (Context context) {
    super.onAttach(context);
    this.context = context;
  }
  public View onCreateView (LayoutInflater inflater, ViewGroup group, Bundle instance) {
    Time time = getArguments().getParcelable("time"); // Just crash if null
    Task[] tasks = (Task[]) getArguments().getParcelableArray("tasks");

    View view = inflater.inflate(R.layout.fragment_week, group, false);

    TextView text = view.findViewById(R.id.text);
    GridView grid = view.findViewById(R.id.grid);

    text.setText("Week " + time.getWeek());
    grid.setAdapter(new WeekGridAdapter(context, time, tasks));
    return view;
  }
}

