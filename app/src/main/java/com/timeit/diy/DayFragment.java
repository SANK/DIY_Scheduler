package com.timeit.diy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import static android.app.Activity.RESULT_OK;

public class DayFragment extends Fragment {
  private Context context;
  private String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
  public static final DayFragment newInstance (Time time, Task[] tasks) {
    DayFragment fragment = new DayFragment();
    Bundle arguments = new Bundle();
    arguments.putParcelable("time", time);
    arguments.putParcelableArray("tasks", tasks);
    fragment.setArguments(arguments);
    return fragment;
  }
  @Override
  public void onAttach (Context context) {
    // Retrive parent context on creation
    super.onAttach(context);
    this.context = context;
  }
  public View onCreateView (LayoutInflater inflater, ViewGroup group, Bundle instance) {
    // Fetch the time and tasks
    final Time time = getArguments().getParcelable("time"); // Just crash if null
    Task[] tasks = (Task[]) getArguments().getParcelableArray("tasks");

    View view = inflater.inflate(R.layout.fragment_day, group, false);

    TextView text = view.findViewById(R.id.text);
    TextView newtask = view.findViewById(R.id.newtask);
    ListView list = view.findViewById(R.id.listview);

    text.setText(days[time.getWeekDay()-1] + "  " + time.getDay() + "/" + (time.getMonth()+1) + "/" + time.getYear());
    list.setAdapter(new TaskListAdapter(context, tasks));
    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

      }
    });
    newtask.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("time", time);
        intent.putExtra("kind", 2); // SWITCH
        intent.putExtra("activity", 9); // LESSON
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
      }
    });
    return view;
  }
}
