package com.timeit.diy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class MonthFragment extends Fragment {
  private static final String[] names = {
    "January" , "February" , "March"     ,
    "April"   , "May"      , "June"      ,
    "July"    , "August"   , "September" ,
    "October" , "November" , "December"
  };
  private Context context;
  public static MonthFragment newInstance (Time time, Task[] tasks) {
    MonthFragment fragment = new MonthFragment();
    Bundle arguments = new Bundle();
    arguments.putParcelable("time", time);
    arguments.putParcelableArray("tasks", tasks);
    fragment.setArguments(arguments);
    return fragment;
  }
  @Override
  public void onAttach (Context context) {
    super.onAttach(context);
    this.context = context;
  }
  @Override
  public View onCreateView (LayoutInflater inflater, ViewGroup group, Bundle instance) {
    Time time = getArguments().getParcelable("time"); // Just crash if null
    Task[] tasks = (Task[]) getArguments().getParcelableArray("tasks");

    View view = inflater.inflate(R.layout.fragment_month, group, false);

    TextView name = view.findViewById(R.id.month);
    GridView grid = view.findViewById(R.id.adapter_grid);
    ListView list = view.findViewById(R.id.adapter_list);

    name.setText(names[time.getMonth()] + " " + String.valueOf(time.getYear()));
    grid.setAdapter(new MonthGridAdapter(context, time, tasks));
    list.setAdapter(new TaskListAdapter(context, tasks));

    return view;
  }
}
